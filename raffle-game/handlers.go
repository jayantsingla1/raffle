package raffle_game

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

func GetToken(w http.ResponseWriter, r *http.Request) {
	log.Info("GetToken call received")

	apiParams := apiParamsFetcher(r)
	userId := apiParams.UserId

	user := getUserFromDB(userId)

	if user.Id == 0 {
		w.WriteHeader(404)
		_ = json.NewEncoder(w).Encode(Notification{Message: UserNotFound})
		return
	}

	user.TokensCount++
	err := updateUserDetails(user)
	if err != nil {
		log.Error("Error while updating user details for user id: ", user.Id)
		_ = json.NewEncoder(w).Encode(Notification{Message: "Error while updating user details"})
		return
	}

	_ = json.NewEncoder(w).Encode(Notification{Message: TokenAllocated})
}

func GetNextEventDetails(w http.ResponseWriter, r *http.Request) {
	log.Info("GetNextEventDetails call received")

	events := getAllLotteryEvents()
	sort.Slice(events, func(i, j int) bool {
		return events[i].Timestamp < events[j].Timestamp
	})

	loc, _ := time.LoadLocation(IndianTimeZone)
	currentTimeStamp := int(time.Now().In(loc).Unix())

	for _, event := range events {
		if event.Timestamp > currentTimeStamp {
			_ = json.NewEncoder(w).Encode(event)
			return
		}
	}
	_ = json.NewEncoder(w).Encode(LotteryEvent{Message: EventNotFound})
}

func EnableUserParticipation(w http.ResponseWriter, r *http.Request) {
	log.Info("EnableUserParticipation call received")
	apiParams := apiParamsFetcher(r)

	userId := apiParams.UserId
	eventId := apiParams.EventId

	var (
		user         User
		lotteryEvent LotteryEvent
		wg           sync.WaitGroup
	)

	wg.Add(2)
	go func() {
		defer wg.Done()
		user = getUserFromDB(userId)
	}()
	go func() {
		defer wg.Done()
		lotteryEvent = getLotteryEventFromDB(eventId)
	}()
	wg.Wait()

	if user.Id == 0 {
		w.WriteHeader(404)
		_ = json.NewEncoder(w).Encode(Notification{Message: UserNotFound})
		return
	}

	if lotteryEvent.Id == 0 {
		w.WriteHeader(404)
		_ = json.NewEncoder(w).Encode(Notification{Message: EventNotFound})
		return
	}

	listOfParticipants := strings.Split(lotteryEvent.Participants, ",")

	for _, participant := range listOfParticipants {
		if participant == strconv.Itoa(userId) {
			_ = json.NewEncoder(w).Encode(Notification{Message: UserAlreadyParticipating})
			return
		}
	}

	if user.TokensCount == 0 {
		_ = json.NewEncoder(w).Encode(Notification{Message: NoToken})
		return
	}

	lotteryEvent.Participants += strconv.Itoa(userId) + ","
	user.TokensCount--

	var (
		userError         error
		lotteryEventError error
		updateWg          sync.WaitGroup
	)

	updateWg.Add(2)
	go func() {
		defer updateWg.Done()
		userError = updateUserDetails(user)
	}()
	go func() {
		defer updateWg.Done()
		lotteryEventError = updateLotteryEventDetails(lotteryEvent)
	}()
	updateWg.Wait()

	if userError != nil {
		log.Error("Failed to update user details")
		_ = json.NewEncoder(w).Encode(Notification{Message: "Failed to update user details"})
		return
	}

	if lotteryEventError != nil {
		log.Error("Failed to update lottery event details")
		_ = json.NewEncoder(w).Encode(Notification{Message: "Failed to update lottery event details"})
		return
	}

	_ = json.NewEncoder(w).Encode(Notification{Message: UserAllowedToParticipate})
}

func GetWinnersRecord(w http.ResponseWriter, r *http.Request) {
	log.Info("GetWinnersRecord call received")

	apiParams := apiParamsFetcher(r)
	oldTimeStamp := getXDaysAgoTimeStamp(apiParams.LastDays)

	loc, _ := time.LoadLocation(IndianTimeZone)
	currentTimeStamp := int(time.Now().In(loc).Unix())

	lotteryEvents := getAllLotteryEvents()
	winnerUserIds := make([]int, 0)
	for _, event := range lotteryEvents {
		if event.Timestamp < currentTimeStamp && event.Timestamp >= oldTimeStamp {
			winnerUserIds = append(winnerUserIds, event.WinnerUserId)
		}
	}

	winners := getUsersFromDB(winnerUserIds)

	_ = json.NewEncoder(w).Encode(WinnersResponse{Winners: winners})
}

func ComputeWinner(w http.ResponseWriter, r *http.Request) {
	log.Info("ComputeWinner call received")

	apiParams := apiParamsFetcher(r)
	eventId := apiParams.EventId

	lotteryEvent := getLotteryEventFromDB(eventId)
	if lotteryEvent.Id == 0 {
		w.WriteHeader(404)
		_ = json.NewEncoder(w).Encode(Notification{Message: EventNotFound})
		return
	}

	loc, _ := time.LoadLocation(IndianTimeZone)
	currentTimeStamp := int(time.Now().In(loc).Unix())

	if lotteryEvent.Timestamp > currentTimeStamp {
		_ = json.NewEncoder(w).Encode(ComputeWinnerResponse{Message: EventNotStarted})
		return
	}

	if len(lotteryEvent.Participants) == 0 {
		_ = json.NewEncoder(w).Encode(ComputeWinnerResponse{Message: NoParticipants})
		return
	}

	if lotteryEvent.WinnerUserId != 0 {
		winner := getUserFromDB(lotteryEvent.WinnerUserId)
		_ = json.NewEncoder(w).Encode(ComputeWinnerResponse{Winner: &winner})
		return
	}

	winner, err := gamePlay(lotteryEvent)
	if err != nil {
		log.Error("Error occurred while computing winner")
		_ = json.NewEncoder(w).Encode(ComputeWinnerResponse{Message: "Error occurred while computing winner"})
		return
	}

	_ = json.NewEncoder(w).Encode(ComputeWinnerResponse{Winner: winner})
	return
}
