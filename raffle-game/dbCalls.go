package raffle_game

import (
	log "github.com/sirupsen/logrus"
)

func getUserFromDB(userId int) User {
	var user User
	query := `select name, phone, tokens_count from grofers.users where id = ?`
	dbResultsIter := cql.Session.Query(query, userId).Iter()

	var (
		name        string
		phone       string
		tokensCount int
	)
	for dbResultsIter.Scan(&name, &phone, &tokensCount) {
		user = User{Id: userId, Name: name, Phone: phone, TokensCount: tokensCount}
	}
	if err := dbResultsIter.Close(); err != nil {
		log.Error("Error while closing cql query iterator and error is ", err)
	}
	return user
}

func getUsersFromDB(userIds []int) []User {
	users := make([]User, 0)
	query := `select id, name, phone, tokens_count from grofers.users where id IN ?`
	dbResultsIter := cql.Session.Query(query, userIds).Iter()

	var (
		id          int
		name        string
		phone       string
		tokensCount int
	)
	for dbResultsIter.Scan(&id, &name, &phone, &tokensCount) {
		user := User{Id: id, Name: name, Phone: phone, TokensCount: tokensCount}
		users = append(users, user)
	}
	if err := dbResultsIter.Close(); err != nil {
		log.Error("Error while closing cql query iterator and error is ", err)
	}
	return users
}

func getLotteryEventFromDB(eventId int) LotteryEvent {
	var lotteryEvent LotteryEvent
	query := `select name, timestamp, award, winner_user_id, participants from grofers.events where id = ?`
	dbResultsIter := cql.Session.Query(query, eventId).Iter()

	var (
		name         string
		timestamp    int
		award        string
		winnerUserId int
		participants string
	)

	for dbResultsIter.Scan(&name, &timestamp, &award, &winnerUserId, &participants) {
		lotteryEvent = LotteryEvent{Id: eventId, Name: name, Award: award, Timestamp: timestamp, WinnerUserId: winnerUserId, Participants: participants}
	}
	if err := dbResultsIter.Close(); err != nil {
		log.Error("Error while closing cql query iterator and error is ", err)
	}
	return lotteryEvent
}

func getAllLotteryEvents() []LotteryEvent {
	lotteryEvents := make([]LotteryEvent, 0)
	query := `select id, name, timestamp, award, winner_user_id, participants from grofers.events`
	dbResultsIter := cql.Session.Query(query).Iter()

	var (
		id           int
		name         string
		timestamp    int
		award        string
		winnerUserId int
		participants string
	)

	for dbResultsIter.Scan(&id, &name, &timestamp, &award, &winnerUserId, &participants) {
		lotteryEvent := LotteryEvent{Id: id, Name: name, Award: award, Timestamp: timestamp, WinnerUserId: winnerUserId, Participants: participants}
		lotteryEvents = append(lotteryEvents, lotteryEvent)
	}
	if err := dbResultsIter.Close(); err != nil {
		log.Error("Error while closing cql query iterator and error is ", err)
	}
	return lotteryEvents
}

func updateUserDetails(user User) error {
	query := "update grofers.users set name = ?, phone = ?, tokens_count = ? where id = ?"
	err := cql.Session.Query(query, user.Name, user.Phone, user.TokensCount, user.Id).Exec()

	return err
}

func updateLotteryEventDetails(event LotteryEvent) error {
	query := "update grofers.events set name = ?, timestamp = ?, award = ?, winner_user_id = ?, participants = ? where id = ?"
	err := cql.Session.Query(query, event.Name, event.Timestamp, event.Award, event.WinnerUserId, event.Participants, event.Id).Exec()

	return err
}