package raffle_game

type Notification struct {
	Message string `json:"message"`
}

type LotteryEvent struct {
	Id           int    `json:"id,omitempty"`
	Name         string `json:"name,omitempty"`
	Award        string `json:"award,omitempty"`
	Timestamp    int    `json:"timestamp,omitempty"`
	WinnerUserId int    `json:"winner_user_id,omitempty"`
	Participants string `json:"participants,omitempty"`
	Message      string `json:"message,omitempty"`
}

type User struct {
	Id          int    `json:"id,omitempty"`
	Name        string `json:"name,omitempty"`
	Phone       string `json:"phone,omitempty"`
	TokensCount int    `json:"tokens_count"`
}

type WinnersResponse struct {
	Winners []User `json:"winners"`
}

type ComputeWinnerResponse struct {
	Winner  *User  `json:"winner,omitempty"`
	Message string `json:"message,omitempty"`
}

type ApiParams struct {
	UserId   int
	EventId  int
	LastDays int
}
