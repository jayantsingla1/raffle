package raffle_game

const (
	TokenAllocated           = "Token allocated to user"
	UserNotFound             = "User is not present in Grofers database"
	EventNotFound            = "Lottery event not found"
	NoToken                  = "No token to participate in the event"
	UserAlreadyParticipating = "This user is already participating in above lottery event"
	UserAllowedToParticipate = "This user is now participating in above lottery event"
	IndianTimeZone           = "Asia/Kolkata"
	EventNotStarted          = "This lottery event is not started yet"
	NoParticipants           = "Lottery event dismissed, because no one participated"
)
