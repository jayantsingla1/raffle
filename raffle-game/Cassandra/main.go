package Cassandra

import (
	"github.com/gocql/gocql"
	log "github.com/sirupsen/logrus"
	"time"
)

const cassandraHost = "127.0.0.1"

type Cassandra struct {
	cluster *gocql.ClusterConfig
	Session *gocql.Session
}

func (cql *Cassandra) Connect() bool {
	var err error
	connected := false
	maxRetry := 5
	retry := 0
	for connected != true {
		if cql.Session == nil || cql.Session.Closed() {
			cql.Session, err = cql.cluster.CreateSession()
			if err != nil {
				if retry == maxRetry {
					return false
				}
				log.Errorf("Couldn't Connect to Cassandra: %s\nRetrying in 5 seconds...attempt %d", err, retry+1)
				time.Sleep(5 * time.Second)
				retry++
			} else {
				connected = true
				log.Info("Cassandra session started...")
			}
		} else {
			connected = true
		}
	}
	return connected
}

func Instance() *Cassandra {
	cql := Cassandra{}
	cql.cluster = gocql.NewCluster(cassandraHost)
	connected := cql.Connect()
	if !connected {
		log.Panic("Could not connect to  cassandra")
	} else {
		log.Info("Connected to cassandra")
	}

	return &cql
}
