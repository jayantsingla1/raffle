package raffle_game

import (
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func apiParamsFetcher(r *http.Request) ApiParams {
	var apiParams ApiParams
	uId := r.URL.Query().Get("userId")
	userId, _ := strconv.Atoi(uId)

	eId := r.URL.Query().Get("eventId")
	eventId, _ := strconv.Atoi(eId)

	lDays := r.URL.Query().Get("lastDays")
	lastDays, _ := strconv.Atoi(lDays)

	apiParams.UserId = userId
	apiParams.EventId = eventId
	apiParams.LastDays = lastDays

	return apiParams
}

func getXDaysAgoTimeStamp(days int) int {
	hours := 24 * days
	loc, _ := time.LoadLocation(IndianTimeZone)
	xDaysAgoTime := time.Now().In(loc).Add(time.Duration(-1*hours) * time.Hour)
	return int(xDaysAgoTime.Unix())
}

func gamePlay(lotteryEvent LotteryEvent) (*User, error) {
	rawParticipants := strings.Split(lotteryEvent.Participants, ",")
	participants := make([]int, 0)

	for _, participant := range rawParticipants {
		intParticipant, _ := strconv.Atoi(participant)

		if intParticipant > 0 {
			participants = append(participants, intParticipant)
		}
	}

	rand.Seed(time.Now().Unix())
	winnerUserId := participants[rand.Intn(len(participants))]

	//Register this winner for lottery event
	lotteryEvent.WinnerUserId = winnerUserId
	err := updateLotteryEventDetails(lotteryEvent)

	if err != nil {
		return nil, err
	}

	winner := getUserFromDB(winnerUserId)
	return &winner, nil
}
