package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"net/http"
	"raffle/raffle-game"
)

func main() {
	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/", healthCheck)
	router.HandleFunc("/getToken", raffle_game.GetToken)
	router.HandleFunc("/getNextEventDetails", raffle_game.GetNextEventDetails)
	router.HandleFunc("/enableUserParticipation", raffle_game.EnableUserParticipation)
	router.HandleFunc("/getWinnersRecord", raffle_game.GetWinnersRecord)
	router.HandleFunc("/computeWinner", raffle_game.ComputeWinner)

	Port := "8040"
	addr := ":" + Port
	log.Info(Port)
	log.Info(addr)
	errHttp := http.ListenAndServe(addr, router) // setting listening port

	if errHttp != nil {
		log.WithFields(log.Fields{
			"addr": addr,
			"err":  errHttp,
		}).Fatal("Unable to create HTTP Service ")
	}
}

type healthCheckResponse struct {
	Status string `json:"status"`
	Code   int    `json:"code"`
}

func healthCheck(w http.ResponseWriter, r *http.Request) {
	log.Info("Raffle Health Check Received")
	json.NewEncoder(w).Encode(healthCheckResponse{Status: "OK: Service is running", Code: 200})
}
