**INTRODUCTION**
This system will allow users to participate in lucky draw events.
1. User should have token to participate in the event.
2. There is no limit on number of tokens per user, but these tokens will be considered as entry pass for any lottery event.
3. After every participation, token balance for that user will be decremented by one.
4. Any random participant will be considered as winner for any lottery event.
TODO:
   We can reiterate over the logic to decide the winner. Instead of choosing the winner randomly, we can use some 
   scores to determine trustworthiness of any user, and use that metric to declare the winner. These scores can be
   number of orders placed by her on grofers, total amount spent, etc.
   

**REQUIREMENTS**
Below mentioned technologies are required to run this system, once you clone it.
1. Golang
2. Cassandra


**DATABASE SCHEMA**
Cassandra is used as database with below two tables.

KeySpace Definition (we can tune the replication_factor according to our usage)
`create keyspace grofers with replication = {'class':'SimpleStrategy', 'replication_factor': 1};`

Table 1: Contains information of Grofers users.

`
CREATE TABLE grofers.users (
    id int,
    name text,
    phone text,
    tokens_count int,
    PRIMARY KEY (id)
);
`

Table 2: Contains information of Grofers lottery events.

`
CREATE TABLE grofers.events (
    id int,
    name text,
    timestamp int,
    award text,
    winner_user_id int,
    participants text,
    PRIMARY KEY (id)
);
`

Data Inflation Queries:
`
insert into grofers.users (id, name, phone, tokens_count) VALUES (4272692, 'Jayant', '7206789999', 4);
insert into grofers.users (id, name, phone, tokens_count) VALUES (4272693, 'Rahul', '7206789969', 3);
insert into grofers.users (id, name, phone, tokens_count) VALUES (4272698, 'Vinay', '7206734999', 2);
insert into grofers.users (id, name, phone, tokens_count) VALUES (4272692, 'Jayant', '7206789999', 4);
insert into grofers.users (id, name, phone, tokens_count) VALUES (4272690, 'Rajat', '8206789999', 7);
`

`
INSERT INTO grofers.events (id, name , timestamp , award , winner_user_id, participants) VALUES (567654, 'Bumper Offer', 1588941247, 'Rs. 500 Grofers Voucher', 4272693, '4272693,4272690,');
INSERT INTO grofers.events (id, name , timestamp , award , winner_user_id, participants) VALUES (567653, 'Friday Bonanza', 1620045247, 'Rs. 200 Grofers Voucher', 4272690, '4272693,4272690,4272692,');
INSERT INTO grofers.events (id, name , timestamp , award , winner_user_id, participants) VALUES (567651, 'Tuesday Bonanza', 1620390847, 'Rs. 100 Grofers Voucher', 4272698, '4272693,4272690,4272698,');
INSERT INTO grofers.events (id, name , timestamp , award ) VALUES (567652, 'Bumper Offer 2', 1622378047, 'Rs. 5000 Grofers Voucher');
INSERT INTO grofers.events (id, name , timestamp , award, participants) VALUES (5671, 'Bumper Offer 3', 1620390847, 'Rs. 50 Grofers Voucher', '4272692,4272690,4272698');
`


**APIs**
`http://localhost:8040/`
This API is just to check whether server is working or not. We can call it health check API.

`http://localhost:8040/getToken?userId=4272690`
This API will add token in user's database so that she can use it to participate in lottery events.

`http://localhost:8040/getNextEventDetails`
This API will provide the details of next lottery event.

`http://localhost:8040/enableUserParticipation?userId=4272698&eventId=567652`
This API will register a user in the participants list of any lottery event. User will be registered only if she has
lottery token.

`http://localhost:8040/getWinnersRecord?lastDays=10`
This API will return list of winners of all the lottery events happened in last x days.

`http://localhost:8040/computeWinner?eventId=5671`
This API will compute the winner for any lottery event.


**About Developer**
_Jayant Singla
jayantsk99@gmail.com_